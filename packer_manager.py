import sys
import getopt
import os
import glob
import time
import json

# import packer executable
from packerpy import PackerExecutable

p = PackerExecutable("/usr/local/bin/packer")


# Functions #
# usage
def usage():
    print("python packer_manager.py -i templatefile.json")


# validate template
def validate_template(t):
    try:
        # validate
        (ret, out, err) = p.validate(t)
        # print(out)
        # print(err)
        # print(ret)

        # print ("template is valid")
        if ret == 0:
            return True
        else:
            return False

    except ValueError:
        raise Exception("template was not valid")


# build image
def buildimage(t):
    try:
        # build
        # p.build(t)
        (ret, out, err) = p.build(t)
        # print(out)

    except ValueError:
        raise Exception("image could not be built")


# get image id
def getimageid():
    image_list = []
    manifest_path = './manifests'

    # wait for manifest file to exist
    while not glob.glob('./manifests/manifest_*'):
        time.sleep(5)

    # loop through filenames in manifest directory, extract image id and return id
    for filename in os.listdir(manifest_path):
        if filename.startswith('manifest_'):
            data = str(manifest_path + "/" + filename)
            with open(data, 'r') as manifest:
                data_store = json.load(manifest)
                image_id = str(data_store["builds"][0]["artifact_id"]).split(':')[1]
                image_list.append(image_id)

    return image_list


# main
def main(argv):
    infile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:", ["ifile="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-i", "--ifile"):
            infile = arg

    if infile is not None:
        # print ("validating template")
        isvalid = validate_template(infile)
        if isvalid:
            print("template valid, building image")
            buildimage(infile)
            print("image built")
            print("getting image ID")
            image_id = getimageid()
            print("the image built was: ", image_id[0])
        else:
            print("template not valid")
            sys.exit()

    else:
        usage()
        sys.exit(2)


if __name__ == "__main__":
    main(sys.argv[1:])
