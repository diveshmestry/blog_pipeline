import getopt
import sys

from python_terraform import *


#return_code, stdout, stderr = tf.plan()
#print(stdout)


# Functions #
# usage
def usage():
    print("")

def tf_plan(template):
    return_code, stdout, stderr = tf.plan()


def tf_apply(template, ami):
    return_code, stdout, stderr = tf.apply()


def validate_files():
    pass

def initialize_tf(tf_path):
    return Terraform(working_dir=tf_path)

# main
def main(argv):
    tf_dir = ''
    try:
        opts, args = getopt.getopt(argv, "hi:", ["ifile="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-i", "--ifile"):
            tf_dir = arg

    if tf_dir is not None:
        tf = initialize_tf(tf_dir)


    else:
        usage()
        sys.exit(2)


if __name__ == "__main__":
    main(sys.argv[1:])
